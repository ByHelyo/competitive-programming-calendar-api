openapi: 3.0.0
servers:
  - url: https://competitiveprogrammingcalendar.agreeabletree-1f3a62a8.eastus.azurecontainerapps.io
info:
  version: 1.0.0
  title: Competitive programming calendar API
paths:
  /api/challenges:
    get:
      summary: Returns all challenges.
      responses:
        '200':
          description: A list of challenges.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/challengeWithId'
        '500':
          $ref: '#/components/responses/internalServerError'
  /api/contests:
    get:
      summary: Returns all contests.
      responses:
        '200':
          description: A list of contests.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/contestWithId'
        '500':
          $ref: '#/components/responses/internalServerError'
  /api/challenge/{challengeId}:
    parameters:
      - $ref: '#/components/parameters/challengeId'
    get:
      summary: Returns a specific challenge.
      description: Returns the challenge corresponding to the provided **challengeId**.
      responses:
        '200':
          description: Challenge corresponding to the provided **challengeId**.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/challengeWithId'
        '400':
          $ref: '#/components/responses/badRequest'
        '500':
          $ref: '#/components/responses/internalServerError'
  /api/contest/{contestId}:
    parameters:
      - $ref: '#/components/parameters/contestId'
    get:
      summary: Returns a specific contest.
      description: Returns the contest corresponding to the provided **contestId**.
      responses:
        '200':
          description: Contest corresponding to the provided **contestId**.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/contestWithId'
        '400':
          $ref: '#/components/responses/badRequest'
        '500':
          $ref: '#/components/responses/internalServerError'
  /admin/challenge:
    post:
      security:
        - BearerAuth: []
      summary: Post challenge
      description: Post challenge with the details described in the request.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/challenge'
      responses:
        '200':
          description: Returns the posted challenge.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/challengeWithId'
        '400':
          $ref: '#/components/responses/badRequest'
        '401':
          $ref: '#/components/responses/unauthorizedError'
        '500':
          $ref: '#/components/responses/internalServerError'
  /admin/contest:
    post:
      security:
        - BearerAuth: []
      summary: Post contest.
      description: Post contest with the details described in the request.
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/contest'
      responses:
        '200':
          description: Returns the posted contest.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/contestWithId'
        '400':
          $ref: '#/components/responses/badRequest'
        '401':
          $ref: '#/components/responses/unauthorizedError'
        '500':
          $ref: '#/components/responses/internalServerError'
  /admin/challenge/{challengeId}:
    parameters:
      - $ref: '#/components/parameters/challengeId'
    delete:
      security:
        - BearerAuth: []
      summary: Returns the delete challenge
      description: Delete challenge corresponding to the provided **challengeId**.
      responses:
        '200':
          description: Returns the deleted challenge.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/challengeWithId'
        '400':
          $ref: '#/components/responses/badRequest'
        '401':
          $ref: '#/components/responses/unauthorizedError'
        '500':
          $ref: '#/components/responses/internalServerError'
  /admin/contest/{contestId}:
    parameters:
      - $ref: '#/components/parameters/contestId'
    delete:
      security:
        - BearerAuth: []
      summary: Returns the delete contest.
      description: Delete contest corresponding to the provided **contestId**.
      responses:
        '200':
          description: Returns the deleted contest.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/contestWithId'
        '400':
          $ref: '#/components/responses/badRequest'
        '401':
          $ref: '#/components/responses/unauthorizedError'
        '500':
          $ref: '#/components/responses/internalServerError'
components:
  parameters:
    contestId:
      name: contestId
      description: The unique identifier of the contest.
      in: path
      required: true
      schema:
        type: integer
        format: int32
    challengeId:
      name: challengeId
      description: The unique identifier of the challenge.
      in: path
      required: true
      schema:
        type: integer
        format: int32
  schemas:
    contestWithId:
      type: object
      properties:
        id:
          type: integer
          format: int32
          description: Contest ID
        name:
          type: string
          description: Contest name.
        start:
          type: string
          format: date-time
          description: Contest start date.
        end:
          type: string
          format: date-time
          description: Contest end date.
        duration:
          type: integer
          format: int32
          description: Contest duration.
        challenge:
          type: string
          description: Contest challenge
        url:
          type: string
          description: Contest url.
    contest:
      type: object
      properties:
        name:
          type: string
          description: Contest name.
        start:
          type: string
          format: date-time
          description: Contest start date.
        end:
          type: string
          format: date-time
          description: Contest end date.
        duration:
          type: integer
          format: int32
          description: Contest duration.
        challenge:
          type: string
          description: Contest challenge
        url:
          type: string
          description: Contest url.
    challengeWithId:
      type: object
      properties:
        id:
          type: integer
          format: int32
          description: Challenge ID.
        name:
          type: string
          description: Challenge name.
    challenge:
      type: object
      properties:
        id:
          type: integer
          format: int32
          description: Challenge ID.
        name:
          type: string
          description: Challenge name.
  responses:
    internalServerError:
      description: Internal server error
      content:
        application/json:
          schema:
            type: object
            properties:
              code:
                type: integer
                description: Code error.
              error:
                type: string
                description: Error description.
    badRequest:
      description: Bad request
      content:
        application/json:
          schema:
            type: object
            properties:
              code:
                type: integer
                description: Code error.
              error:
                type: string
                description: Error description.
    unauthorizedError:
      description: Access token is missing or invalid.
  securitySchemes:
    BearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
