<!-- Generator: Widdershins v4.0.1 -->

<h1 id="competitive-programming-calendar-api">Competitive programming calendar API v1.0.0</h1>

Base URLs:

* <a href="https://competitiveprogrammingcalendar.agreeabletree-1f3a62a8.eastus.azurecontainerapps.io/">https://competitiveprogrammingcalendar.agreeabletree-1f3a62a8.eastus.azurecontainerapps.io</a>

# Authentication

- HTTP Authentication, scheme: bearer 

<h1 id="competitive-programming-calendar-api-default">Default</h1>

## get__api_challenges

> Code samples

`GET /api/challenges`

*Returns all challenges.*

> Example responses

> 200 Response

```json
[
  {
    "id": 0,
    "name": "string"
  }
]
```

<h3 id="get__api_challenges-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|A list of challenges.|Inline|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal server error|Inline|

<h3 id="get__api_challenges-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[challengeWithId](#schemachallengewithid)]|false|none|none|
|» id|integer(int32)|false|none|Challenge ID.|
|» name|string|false|none|Challenge name.|

Status Code **500**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

<aside class="success">
This operation does not require authentication
</aside>

## get__api_contests

> Code samples

`GET /api/contests`

*Returns all contests.*

> Example responses

> 200 Response

```json
[
  {
    "id": 0,
    "name": "string",
    "start": "2019-08-24T14:15:22Z",
    "end": "2019-08-24T14:15:22Z",
    "duration": 0,
    "challenge": "string",
    "url": "string"
  }
]
```

<h3 id="get__api_contests-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|A list of contests.|Inline|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal server error|Inline|

<h3 id="get__api_contests-responseschema">Response Schema</h3>

Status Code **200**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|*anonymous*|[[contestWithId](#schemacontestwithid)]|false|none|none|
|» id|integer(int32)|false|none|Contest ID|
|» name|string|false|none|Contest name.|
|» start|string(date-time)|false|none|Contest start date.|
|» end|string(date-time)|false|none|Contest end date.|
|» duration|integer(int32)|false|none|Contest duration.|
|» challenge|string|false|none|Contest challenge|
|» url|string|false|none|Contest url.|

Status Code **500**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

<aside class="success">
This operation does not require authentication
</aside>

## get__api_challenge_{challengeId}

> Code samples

`GET /api/challenge/{challengeId}`

*Returns a specific challenge.*

Returns the challenge corresponding to the provided **challengeId**.

<h3 id="get__api_challenge_{challengeid}-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|challengeId|path|integer(int32)|true|The unique identifier of the challenge.|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "name": "string"
}
```

<h3 id="get__api_challenge_{challengeid}-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|Challenge corresponding to the provided **challengeId**.|[challengeWithId](#schemachallengewithid)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad request|Inline|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal server error|Inline|

<h3 id="get__api_challenge_{challengeid}-responseschema">Response Schema</h3>

Status Code **400**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

Status Code **500**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

<aside class="success">
This operation does not require authentication
</aside>

## get__api_contest_{contestId}

> Code samples

`GET /api/contest/{contestId}`

*Returns a specific contest.*

Returns the contest corresponding to the provided **contestId**.

<h3 id="get__api_contest_{contestid}-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|contestId|path|integer(int32)|true|The unique identifier of the contest.|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "name": "string",
  "start": "2019-08-24T14:15:22Z",
  "end": "2019-08-24T14:15:22Z",
  "duration": 0,
  "challenge": "string",
  "url": "string"
}
```

<h3 id="get__api_contest_{contestid}-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|Contest corresponding to the provided **contestId**.|[contestWithId](#schemacontestwithid)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad request|Inline|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal server error|Inline|

<h3 id="get__api_contest_{contestid}-responseschema">Response Schema</h3>

Status Code **400**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

Status Code **500**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

<aside class="success">
This operation does not require authentication
</aside>

## post__admin_challenge

> Code samples

`POST /admin/challenge`

*Post challenge*

Post challenge with the details described in the request.

> Body parameter

```json
{
  "id": 0,
  "name": "string"
}
```

<h3 id="post__admin_challenge-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[challenge](#schemachallenge)|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "name": "string"
}
```

<h3 id="post__admin_challenge-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|Returns the posted challenge.|[challengeWithId](#schemachallengewithid)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad request|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Access token is missing or invalid.|None|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal server error|Inline|

<h3 id="post__admin_challenge-responseschema">Response Schema</h3>

Status Code **400**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

Status Code **500**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
BearerAuth
</aside>

## post__admin_contest

> Code samples

`POST /admin/contest`

*Post contest.*

Post contest with the details described in the request.

> Body parameter

```json
{
  "name": "string",
  "start": "2019-08-24T14:15:22Z",
  "end": "2019-08-24T14:15:22Z",
  "duration": 0,
  "challenge": "string",
  "url": "string"
}
```

<h3 id="post__admin_contest-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|body|body|[contest](#schemacontest)|true|none|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "name": "string",
  "start": "2019-08-24T14:15:22Z",
  "end": "2019-08-24T14:15:22Z",
  "duration": 0,
  "challenge": "string",
  "url": "string"
}
```

<h3 id="post__admin_contest-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|Returns the posted contest.|[contestWithId](#schemacontestwithid)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad request|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Access token is missing or invalid.|None|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal server error|Inline|

<h3 id="post__admin_contest-responseschema">Response Schema</h3>

Status Code **400**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

Status Code **500**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
BearerAuth
</aside>

## delete__admin_challenge_{challengeId}

> Code samples

`DELETE /admin/challenge/{challengeId}`

*Returns the delete challenge*

Delete challenge corresponding to the provided **challengeId**.

<h3 id="delete__admin_challenge_{challengeid}-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|challengeId|path|integer(int32)|true|The unique identifier of the challenge.|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "name": "string"
}
```

<h3 id="delete__admin_challenge_{challengeid}-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|Returns the deleted challenge.|[challengeWithId](#schemachallengewithid)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad request|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Access token is missing or invalid.|None|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal server error|Inline|

<h3 id="delete__admin_challenge_{challengeid}-responseschema">Response Schema</h3>

Status Code **400**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

Status Code **500**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
BearerAuth
</aside>

## delete__admin_contest_{contestId}

> Code samples

`DELETE /admin/contest/{contestId}`

*Returns the delete contest.*

Delete contest corresponding to the provided **contestId**.

<h3 id="delete__admin_contest_{contestid}-parameters">Parameters</h3>

|Name|In|Type|Required|Description|
|---|---|---|---|---|
|contestId|path|integer(int32)|true|The unique identifier of the contest.|

> Example responses

> 200 Response

```json
{
  "id": 0,
  "name": "string",
  "start": "2019-08-24T14:15:22Z",
  "end": "2019-08-24T14:15:22Z",
  "duration": 0,
  "challenge": "string",
  "url": "string"
}
```

<h3 id="delete__admin_contest_{contestid}-responses">Responses</h3>

|Status|Meaning|Description|Schema|
|---|---|---|---|
|200|[OK](https://tools.ietf.org/html/rfc7231#section-6.3.1)|Returns the deleted contest.|[contestWithId](#schemacontestwithid)|
|400|[Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1)|Bad request|Inline|
|401|[Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1)|Access token is missing or invalid.|None|
|500|[Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1)|Internal server error|Inline|

<h3 id="delete__admin_contest_{contestid}-responseschema">Response Schema</h3>

Status Code **400**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

Status Code **500**

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|» code|integer|false|none|Code error.|
|» error|string|false|none|Error description.|

<aside class="warning">
To perform this operation, you must be authenticated by means of one of the following methods:
BearerAuth
</aside>

# Schemas

<h2 id="tocS_contestWithId">contestWithId</h2>
<!-- backwards compatibility -->
<a id="schemacontestwithid"></a>
<a id="schema_contestWithId"></a>
<a id="tocScontestwithid"></a>
<a id="tocscontestwithid"></a>

```json
{
  "id": 0,
  "name": "string",
  "start": "2019-08-24T14:15:22Z",
  "end": "2019-08-24T14:15:22Z",
  "duration": 0,
  "challenge": "string",
  "url": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int32)|false|none|Contest ID|
|name|string|false|none|Contest name.|
|start|string(date-time)|false|none|Contest start date.|
|end|string(date-time)|false|none|Contest end date.|
|duration|integer(int32)|false|none|Contest duration.|
|challenge|string|false|none|Contest challenge|
|url|string|false|none|Contest url.|

<h2 id="tocS_contest">contest</h2>
<!-- backwards compatibility -->
<a id="schemacontest"></a>
<a id="schema_contest"></a>
<a id="tocScontest"></a>
<a id="tocscontest"></a>

```json
{
  "name": "string",
  "start": "2019-08-24T14:15:22Z",
  "end": "2019-08-24T14:15:22Z",
  "duration": 0,
  "challenge": "string",
  "url": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|name|string|false|none|Contest name.|
|start|string(date-time)|false|none|Contest start date.|
|end|string(date-time)|false|none|Contest end date.|
|duration|integer(int32)|false|none|Contest duration.|
|challenge|string|false|none|Contest challenge|
|url|string|false|none|Contest url.|

<h2 id="tocS_challengeWithId">challengeWithId</h2>
<!-- backwards compatibility -->
<a id="schemachallengewithid"></a>
<a id="schema_challengeWithId"></a>
<a id="tocSchallengewithid"></a>
<a id="tocschallengewithid"></a>

```json
{
  "id": 0,
  "name": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int32)|false|none|Challenge ID.|
|name|string|false|none|Challenge name.|

<h2 id="tocS_challenge">challenge</h2>
<!-- backwards compatibility -->
<a id="schemachallenge"></a>
<a id="schema_challenge"></a>
<a id="tocSchallenge"></a>
<a id="tocschallenge"></a>

```json
{
  "id": 0,
  "name": "string"
}

```

### Properties

|Name|Type|Required|Restrictions|Description|
|---|---|---|---|---|
|id|integer(int32)|false|none|Challenge ID.|
|name|string|false|none|Challenge name.|

