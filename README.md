# Competitive Programming Calendar API

An API that provides competitive programming contests. It keeps you up-to-date with ongoing and upcoming coding events.

Supported coding events:
- Google Kick Start
- Meta Hacker Cup

> Contests are scraped from:
> - https://codingcompetitions.withgoogle.com/kickstart/schedule
> - https://www.facebook.com/codingcompetitions/hacker-cup/

## Documentation

Documentation available [here](./openapi/openapi.md).
