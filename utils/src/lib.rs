use std::{
    env::{self, VarError},
    process,
};

use sea_orm::{Database, DatabaseConnection, DbErr};

pub fn get_env(env_key: &str) -> String {
    let env: Result<String, VarError> = env::var(env_key);

    if let Err(env) = env {
        log::error!(
            "{} environment variable is not set | Cause: {}",
            env_key,
            env
        );

        process::exit(1);
    }

    env.unwrap()
}

pub async fn setup_database(url: &str) -> DatabaseConnection {
    let db: Result<DatabaseConnection, DbErr> = Database::connect(url).await;

    match db {
        Ok(db) => {
            log::info!("Successfully connected to the database");
            db
        }
        Err(err) => {
            log::error!("Connection to the database failed | Cause: {}", err);
            std::process::exit(1);
        }
    }
}
