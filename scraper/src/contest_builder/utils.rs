use std::future::Future;

use service::presentation::dto::Contest;
use thirtyfour::{
    error::WebDriverResult, prelude::ElementQueryable, By, WebDriver,
    WebElement,
};
use time::{error, format_description::FormatItem, Duration, OffsetDateTime};

pub async fn navigate(driver: &WebDriver, link: &str) -> Result<(), ()> {
    if let Err(err) = driver.goto(link).await {
        log::error!("Navigation to {} failed | Cause: {}", link, err);

        return Err(());
    }

    Ok(())
}

pub async fn query_web_elements_by_css(
    driver: &impl ElementQueryable,
    css: &str,
) -> Result<Vec<WebElement>, ()> {
    let contests: Vec<WebElement> = {
        let ret_contests: WebDriverResult<Vec<WebElement>> =
            driver.query(By::Css(css)).all().await;

        if let Err(err) = ret_contests {
            log::error!("Css \'{}\' is not queryable | Cause: {}", css, err);

            return Err(());
        }

        ret_contests.unwrap()
    };

    Ok(contests)
}

pub async fn extract_str_from_web_elements(
    web_elements: &[WebElement],
    index: usize,
) -> Result<String, ()> {
    let web_element: &WebElement = {
        let ret_web_element: Option<&WebElement> = web_elements.get(index);

        if ret_web_element.is_none() {
            log::error!(
                "index {} is not extractable from web elements (max size: {})",
                index,
                web_elements.len()
            );

            return Err(());
        }

        ret_web_element.unwrap()
    };

    let content: String = {
        let ret_content: WebDriverResult<String> =
            web_element.inner_html().await;

        if let Err(err) = ret_content {
            log::error!(
                "Inner html of web element is not gettable | Cause: {}",
                err
            );

            return Err(());
        }

        ret_content.unwrap()
    };

    Ok(content)
}

pub async fn map_to_contests_from_web_elements<F, Fut>(
    contest_elements: Vec<WebElement>,
    query_contest: F,
) -> Result<Vec<Contest>, ()>
where
    F: Fn(WebElement) -> Fut,
    Fut: Future<Output = Result<Contest, ()>>,
{
    let mut contests: Vec<Contest> = vec![];

    for contest_element in contest_elements {
        contests.push(query_contest(contest_element).await?);
    }

    Ok(contests)
}

pub fn compute_duration(start: OffsetDateTime, end: OffsetDateTime) -> i32 {
    let duration: Duration = end - start;

    duration.whole_hours() as i32
}

pub fn parse_datetime(
    datetime_str: String,
    format: &'static [FormatItem<'static>],
) -> Result<OffsetDateTime, ()> {
    let datetime: OffsetDateTime = {
        let ret_datetime: Result<OffsetDateTime, error::Parse> =
            OffsetDateTime::parse(&datetime_str, &format);

        if let Err(err) = ret_datetime {
            log::error!(
                "\'{}\' is not parsable into datetime | Cause: {}",
                datetime_str,
                err
            );

            return Err(());
        }

        ret_datetime.unwrap()
    };

    Ok(datetime)
}

pub fn compute_end_datetime(
    start: OffsetDateTime,
    duration: i32,
) -> OffsetDateTime {
    start + Duration::new(duration as i64, 0)
}
