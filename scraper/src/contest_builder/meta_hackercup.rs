use std::num::ParseIntError;

use async_trait::async_trait;
use service::presentation::dto::Contest;
use thirtyfour::{
    extensions::query::ElementQueryable, By, WebDriver, WebElement,
};
use time::{macros::format_description, OffsetDateTime};

use crate::contest_builder::{utils, ContestScraper};

const META_HACKERCUP: &str = "Meta Hacker Cup";
const LINK_META_HACKERCUP: &str =
    "https://www.facebook.com/codingcompetitions/hacker-cup/";
const LINK_META_HACKERCUP_CALENDAR: &str =
    "https://www.facebook.com/codingcompetitions/hacker-cup";

const INDEX_NAME: usize = 0;
const INDEX_START_DATETIME: usize = 1;
const INDEX_DURATION: usize = 2;

#[derive(Default)]
pub struct MetaHackerCupScraper {}

#[async_trait]
impl ContestScraper for MetaHackerCupScraper {
    async fn execute(&self, driver: &WebDriver) -> Result<Vec<Contest>, ()> {
        utils::navigate(driver, LINK_META_HACKERCUP_CALENDAR).await?;
        Self::skip_cookies_popup(driver).await?;

        let contests: Vec<WebElement> = utils::query_web_elements_by_css(
            &driver.handle,
            "div._20nr:nth-child(4) > div:nth-child(1) > div:nth-child(1) \
             a.x1ggr2jv",
        )
        .await?;

        let mut res_contests: Vec<Contest> = vec![];

        res_contests.extend(
            utils::map_to_contests_from_web_elements(
                contests,
                Self::query_contest,
            )
            .await?,
        );

        Ok(res_contests)
    }
}

impl MetaHackerCupScraper {
    async fn skip_cookies_popup(driver: &WebDriver) -> Result<(), ()> {
        let button: WebElement = {
            match driver
                .query(By::Css("button[title=\"Only allow essential cookies\""))
                .first()
                .await
            {
                Err(err) => {
                    log::error!(
                        "Cookie button is not queryable | Cause: {}",
                        err
                    );

                    return Err(());
                }
                Ok(button) => button,
            }
        };

        match button.click().await {
            Err(err) => {
                log::error!("Button is not clickable | Cause: {}", err);

                Err(())
            }
            Ok(()) => Ok(()),
        }
    }

    async fn query_contest(contest_element: WebElement) -> Result<Contest, ()> {
        let divs: Vec<WebElement> =
            utils::query_web_elements_by_css(&contest_element, "div.ellipsis")
                .await?;

        let name: String =
            utils::extract_str_from_web_elements(&divs, INDEX_NAME).await?;
        let start: OffsetDateTime = Self::query_start_datetime(&divs).await?;
        let duration: i32 = Self::query_duration(&divs).await?;
        let end: OffsetDateTime = utils::compute_end_datetime(start, duration);

        let contest: Contest = Contest {
            name,
            start,
            end,
            duration,
            challenge: META_HACKERCUP.to_string(),
            url: LINK_META_HACKERCUP.to_string(),
        };

        log::info!("New Contest {:?}", contest);

        Ok(contest)
    }

    async fn query_start_datetime(
        divs: &[WebElement],
    ) -> Result<OffsetDateTime, ()> {
        let mut start_datetime: String = {
            let div_start_datetime: String =
                utils::extract_str_from_web_elements(
                    divs,
                    INDEX_START_DATETIME,
                )
                .await?;

            let ret_start_datetime: Option<&str> =
                div_start_datetime.split(" <").next();

            if ret_start_datetime.is_none() {
                log::error!(
                    "\'{}\' is not splittable into datetime",
                    div_start_datetime
                );

                return Err(());
            }

            ret_start_datetime.unwrap().to_string()
        };

        start_datetime.push_str(" -01");

        let format = format_description!(
            "[weekday repr:long case_sensitive:false], [month repr:long \
             case_sensitive:false] [day padding:none], [year repr:full \
             base:calendar sign:automatic padding:none] at [hour repr:24 \
             padding:none]:[minute padding:zero] [period case:upper] \
             [offset_hour sign:mandatory padding:zero]"
        );

        utils::parse_datetime(start_datetime, format)
    }

    async fn query_duration(divs: &[WebElement]) -> Result<i32, ()> {
        let duration_div: String =
            utils::extract_str_from_web_elements(divs, INDEX_DURATION).await?;

        let duration: &str = {
            let ret_duration: Option<&str> =
                duration_div.split_whitespace().nth(1);

            if ret_duration.is_none() {
                log::error!(
                    "Duration is not extractable from \'{}\'",
                    duration_div
                );

                return Err(());
            }

            ret_duration.unwrap()
        };

        let duration: i32 = {
            let ret_duration: Result<i32, ParseIntError> = duration.parse();

            if let Err(err) = &ret_duration {
                log::error!(
                    "\'{}\' is not parsable into duration | Cause: {}",
                    duration,
                    err
                );
                return Err(());
            }

            ret_duration.unwrap()
        };

        Ok(duration)
    }
}
