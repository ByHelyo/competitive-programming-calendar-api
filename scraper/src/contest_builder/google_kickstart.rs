use async_trait::async_trait;
use service::presentation::dto::Contest;
use thirtyfour::{WebDriver, WebElement};
use time::{macros::format_description, OffsetDateTime};

use crate::contest_builder::{utils, ContestScraper};

const GOOGLE_KICKSTART: &str = "Google Kick Start";
const LINK_GOOGLE_KICKSTART: &str =
    "https://codingcompetitions.withgoogle.com/kickstart";
const LINK_GOOGLE_KICKSTART_CALENDAR: &str =
    "https://codingcompetitions.withgoogle.com/kickstart/schedule";

const INDEX_NAME: usize = 0;
const INDEX_START_DATETIME: usize = 1;
const INDEX_END_DATETIME: usize = 3;

#[derive(Default)]
pub struct GoogleKickStartScraper {}

#[async_trait]
impl ContestScraper for GoogleKickStartScraper {
    async fn execute(&self, driver: &WebDriver) -> Result<Vec<Contest>, ()> {
        utils::navigate(driver, LINK_GOOGLE_KICKSTART_CALENDAR).await?;

        let past_contests: Vec<WebElement> = utils::query_web_elements_by_css(
            &driver.handle,
            "div.schedule-row.schedule-row__past",
        )
        .await?;
        let upcoming_contests: Vec<WebElement> =
            utils::query_web_elements_by_css(
                &driver.handle,
                "div.schedule-row.schedule-row__upcoming",
            )
            .await?;

        let mut contests: Vec<Contest> = vec![];

        contests.extend(
            utils::map_to_contests_from_web_elements(
                past_contests,
                Self::query_contest,
            )
            .await?,
        );
        contests.extend(
            utils::map_to_contests_from_web_elements(
                upcoming_contests,
                Self::query_contest,
            )
            .await?,
        );

        Ok(contests)
    }
}

impl GoogleKickStartScraper {
    async fn query_contest(contest_element: WebElement) -> Result<Contest, ()> {
        let spans: Vec<WebElement> =
            utils::query_web_elements_by_css(&contest_element, "span").await?;

        let name: String =
            utils::extract_str_from_web_elements(&spans, INDEX_NAME).await?;
        let start: OffsetDateTime =
            Self::query_datetime(&spans, INDEX_START_DATETIME).await?;
        let end: OffsetDateTime =
            Self::query_datetime(&spans, INDEX_END_DATETIME).await?;
        let duration: i32 = utils::compute_duration(start, end);

        let contest: Contest = Contest {
            name,
            start,
            end,
            duration,
            challenge: GOOGLE_KICKSTART.to_string(),
            url: LINK_GOOGLE_KICKSTART.to_string(),
        };

        log::info!("New contest: {:?}", contest);

        Ok(contest)
    }

    async fn query_datetime(
        spans: &[WebElement],
        index: usize,
    ) -> Result<OffsetDateTime, ()> {
        let mut datetime: String =
            utils::extract_str_from_web_elements(spans, index).await?;

        datetime.push_str(" +00");

        let format = format_description!(
            "[month repr:short case_sensitive:false] [day padding:none] [year \
             repr:full base:calendar sign:automatic padding:none], [hour \
             repr:24 padding:zero]:[minute padding:zero] [offset_hour \
             sign:mandatory padding:zero]"
        );

        utils::parse_datetime(datetime, format)
    }
}
