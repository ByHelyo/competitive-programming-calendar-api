mod google_kickstart;
mod meta_hackercup;
mod utils;

use async_trait::async_trait;
use service::presentation::dto::Contest;
use thirtyfour::{
    error::WebDriverResult, ChromeCapabilities, DesiredCapabilities, WebDriver,
};

#[derive(Debug)]
pub struct ContestBuilder {}

#[async_trait]
trait ContestScraper {
    async fn execute(&self, driver: &WebDriver) -> Result<Vec<Contest>, ()>;
}

impl ContestBuilder {
    pub async fn get_contests(
        server_url: &str,
    ) -> Result<Vec<Contest>, String> {
        let contests: Result<Vec<Contest>, ()> =
            match ContestBuilder::scrape_contests(server_url).await {
                Ok(contests) => contests,
                Err(error) => {
                    let error_msg: String = format!(
                        "Webdriver creation or close failed | Cause: {}",
                        error
                    );
                    log::error!("{}", error_msg);

                    return Err(error_msg);
                }
            };

        match contests {
            Ok(contests) => Ok(contests),
            Err(()) => {
                Err("An error occurred during the scraping.".to_string())
            }
        }
    }

    async fn scrape_contests(
        server_url: &str,
    ) -> WebDriverResult<Result<Vec<Contest>, ()>> {
        let mut valid: bool = true;

        let challenges: Vec<Box<dyn ContestScraper>> = vec![
            Box::<google_kickstart::GoogleKickStartScraper>::default(),
            Box::<meta_hackercup::MetaHackerCupScraper>::default(),
        ];

        let caps: ChromeCapabilities = DesiredCapabilities::chrome();
        let driver: WebDriver = WebDriver::new(server_url, caps).await?;
        log::info!("Web driver created");

        let mut res_contests: Vec<Contest> = vec![];

        for challenge in challenges {
            let contests: Vec<Contest> = {
                let ret_contests: Result<Vec<Contest>, ()> =
                    challenge.execute(&driver).await;

                if ret_contests.is_err() {
                    valid = false;
                    break;
                }

                ret_contests.unwrap()
            };

            res_contests.extend(contests);
        }

        driver.quit().await?;
        log::info!("Web driver correctly closed");

        if !valid {
            log::error!("Stop scraping");

            return Ok(Err(()));
        }

        Ok(Ok(res_contests))
    }
}
