mod config;

use reqwest::{Client, Error, Response};
use scraper::contest_builder::ContestBuilder;
use service::presentation::dto::Contest;

use crate::config::Config;

#[tokio::main]
async fn main() {
    env_logger::init();
    dotenv::dotenv().ok();

    let mut config: Config = Config::default();

    config.service_url.push_str("/admin/contest");

    let contests: Vec<Contest> = {
        let ret_contests: Result<Vec<Contest>, String> =
            ContestBuilder::get_contests(&config.selenium_url).await;

        if ret_contests.is_err() {
            std::process::exit(1);
        }

        ret_contests.unwrap()
    };

    let client: Client = Client::new();

    for contest in contests.iter() {
        let res: Result<Response, Error> = client
            .post(&config.service_url)
            .bearer_auth(&config.token)
            .json(contest)
            .send()
            .await;

        if let Err(err) = res {
            log::error!("Post failed | Cause: {}", err);
        } else {
            log::info!("Success post");
        }
    }
}
