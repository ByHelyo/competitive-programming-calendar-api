pub struct Config {
    pub selenium_url: String,
    pub service_url: String,
    pub token: String,
}

impl Default for Config {
    fn default() -> Self {
        Self::config_env()
    }
}

impl Config {
    fn config_env() -> Self {
        let selenium_url: String = utils::get_env("SELENIUM_URL");
        let service_url: String = utils::get_env("SERVICE_URL");
        let token: String = utils::get_env("TOKEN");

        Config {
            selenium_url,
            service_url,
            token,
        }
    }
}
