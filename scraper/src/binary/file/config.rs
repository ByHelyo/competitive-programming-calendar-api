use clap::Parser;

const OUTPUT_FILE_DEFAULT: &str = "./assets/contest.json";

pub struct Config {
    pub selenium_url: String,
    pub output_file: String,
}

impl Default for Config {
    fn default() -> Self {
        let cli = Cli::parse();

        let output_file: String = {
            if let Some(output_file) = cli.output.as_ref() {
                output_file.to_string()
            } else {
                OUTPUT_FILE_DEFAULT.to_string()
            }
        };

        Self::config_env(output_file)
    }
}

impl Config {
    fn config_env(output_file: String) -> Self {
        let db_url: String = utils::get_env("SELENIUM_URL");

        Config {
            selenium_url: db_url,
            output_file,
        }
    }
}

#[derive(Parser, Debug)]
struct Cli {
    #[arg(short, long)]
    output: Option<String>,
}
