mod config;

use scraper::contest_builder::ContestBuilder;
use service::presentation::dto::Contest;

use crate::config::Config;

#[tokio::main]
async fn main() {
    env_logger::init();
    dotenv::dotenv().ok();

    let config: Config = Config::default();

    let contests: Vec<Contest> = {
        let ret_contests: Result<Vec<Contest>, String> =
            ContestBuilder::get_contests(&config.selenium_url).await;

        if ret_contests.is_err() {
            std::process::exit(1);
        }

        ret_contests.unwrap()
    };

    match serde_json::to_string_pretty(&contests) {
        Ok(contests_json) => {
            if let Err(error) =
                std::fs::write(&config.output_file, contests_json)
            {
                log::error!(
                    "Failed to write in \'{}\' | Cause: {}",
                    config.output_file,
                    error
                );
            }
        }
        Err(error) => log::error!("Failed to serialize | Cause: {}", error),
    }
}
