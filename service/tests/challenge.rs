use actix_web::{http, web, HttpResponse};
use models::*;
use sea_orm::{DatabaseBackend, MockDatabase, MockExecResult};
use service::{
    presentation::{dto, resource::Resource},
    state::AppState,
};

#[actix_web::test]
async fn test_get_challenges() {
    let db = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results::<challenge::Model>(vec![vec![]])
        .into_connection();
    let state = AppState { db };

    let resp: HttpResponse = Resource::get_challenges(web::Data::new(state))
        .await
        .unwrap();

    assert_eq!(resp.status(), http::StatusCode::OK);
}

#[actix_web::test]
async fn test_get_challenge_by_id() {
    let challenge_name: String = "my_challenge".to_string();
    let challenge_id: i32 = 10;

    let db = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results::<challenge::Model>(vec![vec![
            challenge::Model {
                id: challenge_id,
                name: challenge_name,
            },
        ]])
        .into_connection();
    let state = AppState { db };

    let resp: HttpResponse = Resource::get_challenge_by_id(
        web::Data::new(state),
        challenge_id.into(),
    )
    .await
    .unwrap();

    assert_eq!(resp.status(), http::StatusCode::OK);
}

#[actix_web::test]
async fn test_post_challenge() {
    let challenge_name: String = "my_challenge".to_string();
    let challenge_id: i32 = 1;

    let db = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results::<challenge::Model>(vec![
            vec![],
            vec![challenge::Model {
                id: challenge_id,
                name: challenge_name.clone(),
            }],
        ])
        .into_connection();
    let state = AppState { db };

    let challenge: dto::Challenge = dto::Challenge {
        name: challenge_name,
    };

    let resp: HttpResponse =
        Resource::post_challenge(web::Data::new(state), web::Json(challenge))
            .await
            .unwrap();

    assert_eq!(resp.status(), http::StatusCode::CREATED);
}

#[actix_web::test]
async fn test_delete_challenge() {
    let challenge_name: String = "my_challenge".to_string();
    let challenge_id: i32 = 10;

    let db = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results::<challenge::Model>(vec![vec![
            challenge::Model {
                id: challenge_id,
                name: challenge_name,
            },
        ]])
        .append_exec_results(vec![MockExecResult {
            last_insert_id: 100,
            rows_affected: 1,
        }])
        .into_connection();
    let state = AppState { db };

    let resp = Resource::delete_challenge_by_id(
        web::Data::new(state),
        challenge_id.into(),
    )
    .await
    .unwrap();

    assert_eq!(resp.status(), http::StatusCode::OK);
}
