use actix_web::{http, web, HttpResponse};
use models::*;
use sea_orm::{DatabaseBackend, MockDatabase, MockExecResult};
use service::{
    presentation::{dto, resource::Resource},
    state::AppState,
};
use time::macros::datetime;

#[actix_web::test]
async fn test_get_contests() {
    let db = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results::<challenge::Model>(vec![vec![]])
        .into_connection();
    let state = AppState { db };

    let resp: HttpResponse =
        Resource::get_contests(web::Data::new(state)).await.unwrap();

    assert_eq!(resp.status(), http::StatusCode::OK);
}

#[actix_web::test]
async fn test_get_contest_by_id() {
    let contest_id: i32 = 20;
    let contest_name: String = "my contest".to_string();
    let start = datetime!(1970-01-01 0:00 UTC);
    let end = datetime!(1970-01-01 3:00 UTC);
    let duration: i32 = 180;
    let challenge_id: i32 = 10;
    let challenge_name: String = "the challenge".to_string();
    let url: String = "my url".to_string();

    let db = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results::<contest::Model>(vec![vec![contest::Model {
            id: contest_id,
            name: contest_name,
            start,
            end,
            duration,
            challenge_id,
            url,
        }]])
        .append_query_results::<challenge::Model>(vec![vec![
            challenge::Model {
                id: challenge_id,
                name: challenge_name,
            },
        ]])
        .into_connection();
    let state = AppState { db };

    let resp =
        Resource::get_contest_by_id(web::Data::new(state), contest_id.into())
            .await
            .unwrap();

    assert_eq!(resp.status(), http::StatusCode::OK);
}

#[actix_web::test]
async fn test_post_contest() {
    let challenge_name: String = "my_challenge".to_string();
    let contest_id: i32 = 1;
    let contest_name: String = "my contest".to_string();
    let start = datetime!(1970-01-01 0:00 UTC);
    let end = datetime!(1970-01-01 3:00 UTC);
    let duration: i32 = 180;
    let challenge_id: i32 = 10;
    let url: String = "my url".to_string();

    let db = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results::<challenge::Model>(vec![vec![
            challenge::Model {
                id: challenge_id,
                name: challenge_name.clone(),
            },
        ]])
        .append_query_results::<contest::Model>(vec![
            vec![],
            vec![contest::Model {
                id: contest_id,
                name: contest_name.clone(),
                start,
                end,
                duration,
                challenge_id,
                url: url.clone(),
            }],
        ])
        .into_connection();
    let state = AppState { db };

    let contest: dto::Contest = dto::Contest {
        name: contest_name,
        start: start,
        end: end,
        duration,
        challenge: challenge_name,
        url,
    };

    let resp: HttpResponse =
        Resource::post_contest(web::Data::new(state), web::Json(contest))
            .await
            .unwrap();

    assert_eq!(resp.status(), http::StatusCode::CREATED);
}

#[actix_web::test]
async fn test_delete_contest() {
    let challenge_name: String = "my_challenge".to_string();
    let contest_id: i32 = 1;
    let contest_name: String = "my contest".to_string();
    let start = datetime!(1970-01-01 0:00 UTC);
    let end = datetime!(1970-01-01 3:00 UTC);
    let duration: i32 = 180;
    let challenge_id: i32 = 10;
    let url: String = "my url".to_string();

    let db = MockDatabase::new(DatabaseBackend::Postgres)
        .append_query_results::<contest::Model>(vec![vec![contest::Model {
            id: contest_id,
            name: contest_name,
            start,
            end,
            duration,
            challenge_id,
            url,
        }]])
        .append_exec_results(vec![MockExecResult {
            last_insert_id: 100,
            rows_affected: 1,
        }])
        .append_query_results::<challenge::Model>(vec![vec![
            challenge::Model {
                id: challenge_id,
                name: challenge_name,
            },
        ]])
        .into_connection();
    let state = AppState { db };

    let resp: HttpResponse = Resource::delete_contest_by_id(
        web::Data::new(state),
        contest_id.into(),
    )
    .await
    .unwrap();

    assert_eq!(resp.status(), http::StatusCode::OK);
}
