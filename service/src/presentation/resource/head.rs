use actix_web::{HttpResponse, Responder};

use crate::presentation::resource::Resource;

impl Resource {
    pub async fn head() -> impl Responder {
        HttpResponse::Ok()
    }
}
