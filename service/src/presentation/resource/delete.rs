use actix_web::{web, HttpResponse};

use crate::{
    domain::{entity, service::Service},
    error::ServiceError,
    presentation::{dto, resource::Resource},
    state::AppState,
};

impl Resource {
    pub async fn delete_challenge_by_id(
        data: web::Data<AppState>,
        path: web::Path<i32>,
    ) -> Result<HttpResponse, ServiceError> {
        let response_entity: entity::Challenge =
            Service::delete_challenge_by_id(&data.db, path.into_inner())
                .await?;

        log::info!("Challenge id \'{}\' deleted", response_entity.id.unwrap());

        Ok(HttpResponse::Ok()
            .json(dto::ChallengeWithId::from_entity(response_entity)))
    }

    pub async fn delete_contest_by_id(
        data: web::Data<AppState>,
        path: web::Path<i32>,
    ) -> Result<HttpResponse, ServiceError> {
        let response_entity: entity::Contest =
            Service::delete_contest_by_id(&data.db, path.into_inner()).await?;

        log::info!("Contest id \'{}\' deleted", response_entity.id.unwrap());

        Ok(HttpResponse::Ok()
            .json(dto::ContestWithId::from_entity(response_entity)))
    }
}
