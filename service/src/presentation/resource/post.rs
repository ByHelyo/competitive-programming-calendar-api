use actix_web::{web, HttpResponse};

use crate::{
    domain::{entity, service::Service},
    error::ServiceError,
    presentation::{dto, resource::Resource},
    state::AppState,
};

impl Resource {
    pub async fn post_challenge(
        data: web::Data<AppState>,
        body: web::Json<dto::Challenge>,
    ) -> Result<HttpResponse, ServiceError> {
        let response_entity: entity::Challenge =
            Service::post_challenge(&data.db, body.name.clone()).await?;

        log::info!(
            "Challenge \'{}\' added to the database",
            response_entity.name.as_ref().unwrap()
        );

        let response_dto: dto::ChallengeWithId =
            dto::ChallengeWithId::from_entity(response_entity);
        Ok(HttpResponse::Created().json(response_dto))
    }

    pub async fn post_contest(
        data: web::Data<AppState>,
        body: web::Json<dto::Contest>,
    ) -> Result<HttpResponse, ServiceError> {
        let response_dto: dto::ContestWithId =
            Service::post_contest(&data.db, body.into_inner()).await?;

        log::info!("Contest \'{}\' added to the database", response_dto.name);

        Ok(HttpResponse::Created().json(response_dto))
    }
}
