use actix_web::{web, HttpResponse};

use crate::{
    domain::{entity, service::Service},
    error::ServiceError,
    presentation::{dto, resource::Resource},
    state::AppState,
};

impl Resource {
    pub async fn get_challenges(
        data: web::Data<AppState>,
    ) -> Result<HttpResponse, ServiceError> {
        let challenges: Vec<entity::Challenge> =
            Service::get_challenges(&data.db).await?;

        log::info!("Get challenges from the database");

        Ok(HttpResponse::Ok()
            .json(dto::ChallengeWithId::from_entities(challenges)))
    }

    pub async fn get_challenge_by_id(
        data: web::Data<AppState>,
        path: web::Path<i32>,
    ) -> Result<HttpResponse, ServiceError> {
        let response_challenge: entity::Challenge =
            Service::get_challenge_by_id(&data.db, path.into_inner()).await?;

        log::info!(
            "Get challenge id \'{}\' from the database",
            response_challenge.id.unwrap()
        );

        Ok(HttpResponse::Ok()
            .json(dto::ChallengeWithId::from_entity(response_challenge)))
    }

    pub async fn get_contests(
        data: web::Data<AppState>,
    ) -> Result<HttpResponse, ServiceError> {
        let contests: Vec<entity::Contest> =
            Service::get_contests(&data.db).await?;

        log::info!("Get contests from the database");

        Ok(
            HttpResponse::Ok()
                .json(dto::ContestWithId::from_entities(contests)),
        )
    }

    pub async fn get_contest_by_id(
        data: web::Data<AppState>,
        path: web::Path<i32>,
    ) -> Result<HttpResponse, ServiceError> {
        let response_contest: entity::Contest =
            Service::get_contest_by_id(&data.db, path.into_inner()).await?;

        log::info!(
            "Get challenge id \'{}\' from the database",
            response_contest.id.unwrap()
        );

        Ok(HttpResponse::Ok()
            .json(dto::ContestWithId::from_entity(response_contest)))
    }
}
