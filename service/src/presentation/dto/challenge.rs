use serde::{Deserialize, Serialize};

use crate::domain::entity;

#[derive(Serialize, Deserialize)]
pub struct Challenge {
    pub name: String,
}

#[derive(Serialize)]
pub struct ChallengeWithId {
    pub id: i32,
    pub name: String,
}

impl ChallengeWithId {
    pub fn from_entity(challenge_entity: entity::Challenge) -> Self {
        Self {
            id: challenge_entity.id.unwrap(),
            name: challenge_entity.name.unwrap(),
        }
    }

    pub fn from_entities(
        challenge_entities: Vec<entity::Challenge>,
    ) -> Vec<Self> {
        challenge_entities
            .into_iter()
            .map(Self::from_entity)
            .collect()
    }
}
