use serde::{Deserialize, Serialize};
use time::OffsetDateTime;

use crate::domain::entity;

#[derive(Serialize, Deserialize, Debug)]
pub struct Contest {
    pub name: String,
    #[serde(with = "time::serde::rfc3339")]
    pub start: OffsetDateTime,
    #[serde(with = "time::serde::rfc3339")]
    pub end: OffsetDateTime,
    pub duration: i32,
    pub challenge: String,
    pub url: String,
}

#[derive(Serialize)]
pub struct ContestWithId {
    pub id: i32,
    pub name: String,
    #[serde(with = "time::serde::rfc3339")]
    pub start: OffsetDateTime,
    #[serde(with = "time::serde::rfc3339")]
    pub end: OffsetDateTime,
    pub duration: i32,
    pub challenge: String,
    pub url: String,
}

impl ContestWithId {
    pub fn from_entity(contest: entity::Contest) -> Self {
        Self {
            id: contest.id.unwrap(),
            name: contest.name.unwrap(),
            start: contest.start.unwrap(),
            end: contest.end.unwrap(),
            duration: contest.duration.unwrap(),
            challenge: contest.challenge_name.unwrap(),
            url: contest.url.unwrap(),
        }
    }

    pub fn from_entities(contests: Vec<entity::Contest>) -> Vec<Self> {
        let mut contests_dto: Vec<Self> = Vec::new();

        for contest_entity in contests.into_iter() {
            contests_dto.push(Self::from_entity(contest_entity));
        }

        contests_dto
    }
}
