mod challenge;
mod contest;

pub use challenge::{Challenge, ChallengeWithId};
pub use contest::{Contest, ContestWithId};
