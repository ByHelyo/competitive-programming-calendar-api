use sea_orm::DatabaseConnection;

#[cfg_attr(feature = "bin", derive(Clone))]
pub struct AppState {
    pub db: DatabaseConnection,
}
