use models::{prelude::*, *};
use sea_orm::{
    ActiveValue, DatabaseConnection, DbErr, EntityTrait, InsertResult,
};

use crate::{
    domain::entity,
    error::{ServiceError, ServiceErrorType},
    repository::Repository,
};

impl Repository {
    pub async fn post_challenge(
        db: &DatabaseConnection,
        challenge_name: String,
    ) -> Result<i32, ServiceError> {
        let challenge_model: challenge::ActiveModel = challenge::ActiveModel {
            name: ActiveValue::Set(challenge_name),
            ..Default::default()
        };

        let inserted_challenge: InsertResult<challenge::ActiveModel> = {
            let ret_inserted_challenge: Result<
                InsertResult<challenge::ActiveModel>,
                DbErr,
            > = Challenge::insert(challenge_model).exec(db).await;

            if let Err(err) = ret_inserted_challenge {
                log::error!("Database error | Cause: {}", err);

                return Err(ServiceError::new_without_message(
                    ServiceErrorType::InternalServerError,
                ));
            }

            ret_inserted_challenge.unwrap()
        };

        Ok(inserted_challenge.last_insert_id)
    }

    pub async fn post_contest(
        db: &DatabaseConnection,
        request_entity: &entity::Contest,
    ) -> Result<i32, ServiceError> {
        let contest_model: contest::ActiveModel = contest::ActiveModel {
            name: ActiveValue::Set(
                request_entity.name.as_ref().unwrap().clone(),
            ),
            start: ActiveValue::Set(request_entity.start.unwrap()),
            end: ActiveValue::Set(request_entity.end.unwrap()),
            duration: ActiveValue::Set(request_entity.duration.unwrap()),
            challenge_id: ActiveValue::Set(
                request_entity.challenge_id.unwrap(),
            ),
            url: ActiveValue::Set(request_entity.url.as_ref().unwrap().clone()),
            ..Default::default()
        };

        let inserted_contest: InsertResult<contest::ActiveModel> = {
            let ret_inserted_contest: Result<
                InsertResult<contest::ActiveModel>,
                DbErr,
            > = Contest::insert(contest_model).exec(db).await;

            if let Err(err) = ret_inserted_contest {
                log::error!("Database error | Cause: {}", err);

                return Err(ServiceError::new_without_message(
                    ServiceErrorType::InternalServerError,
                ));
            }

            ret_inserted_contest.unwrap()
        };

        Ok(inserted_contest.last_insert_id)
    }
}
