use models::{prelude::*, *};
use sea_orm::{
    ColumnTrait, DatabaseConnection, DbErr, EntityTrait, QueryFilter,
};

use crate::{
    domain::entity,
    error::{ServiceError, ServiceErrorType},
    repository::Repository,
};

impl Repository {
    pub async fn find_challenges(
        db: &DatabaseConnection,
    ) -> Result<Vec<entity::Challenge>, ServiceError> {
        let challenge_models: Vec<challenge::Model> = {
            let ret_challenge_models: Result<Vec<challenge::Model>, DbErr> =
                Challenge::find().all(db).await;

            if let Err(err) = ret_challenge_models {
                log::error!("Database error | Cause: {}", err);

                return Err(ServiceError::new_without_message(
                    ServiceErrorType::InternalServerError,
                ));
            }

            ret_challenge_models.unwrap()
        };

        Ok(entity::Challenge::from_models(challenge_models))
    }

    pub async fn find_contests(
        db: &DatabaseConnection,
    ) -> Result<Vec<entity::Contest>, ServiceError> {
        let contest_models: Vec<contest::Model> = {
            let ret_contest_models: Result<Vec<contest::Model>, DbErr> =
                Contest::find().all(db).await;

            if let Err(err) = ret_contest_models {
                log::error!("Database error | Cause: {}", err);

                return Err(ServiceError::new_without_message(
                    ServiceErrorType::InternalServerError,
                ));
            }

            ret_contest_models.unwrap()
        };

        Ok(entity::Contest::from_models(contest_models))
    }

    pub async fn find_challenge_by_name(
        db: &DatabaseConnection,
        name: String,
    ) -> Result<Option<entity::Challenge>, ServiceError> {
        let challenge_model: Option<challenge::Model> = {
            let ret_challenge_model: Result<Option<challenge::Model>, DbErr> =
                Challenge::find()
                    .filter(challenge::Column::Name.eq(name))
                    .one(db)
                    .await;

            if let Err(err) = ret_challenge_model {
                log::error!("Database error | Cause: {}", err);

                return Err(ServiceError::new_without_message(
                    ServiceErrorType::InternalServerError,
                ));
            }

            ret_challenge_model.unwrap()
        };

        match challenge_model {
            Some(challenge_model) => {
                let response_entity: entity::Challenge =
                    entity::Challenge::from_model(challenge_model);

                Ok(Some(response_entity))
            }
            None => Ok(None),
        }
    }

    pub async fn contest_exist(
        db: &DatabaseConnection,
        contest_entity: &entity::Contest,
    ) -> Result<Option<entity::Contest>, ServiceError> {
        let contest_model: Option<contest::Model> = {
            let ret_contest_model: Result<Option<contest::Model>, DbErr> =
                Contest::find()
                    .filter(
                        contest::Column::Name.eq(contest_entity
                            .name
                            .as_ref()
                            .unwrap()
                            .clone()),
                    )
                    .filter(contest::Column::Start.eq(contest_entity.start))
                    .filter(
                        contest::Column::Duration.eq(contest_entity.duration),
                    )
                    .filter(
                        contest::Column::ChallengeId
                            .eq(contest_entity.challenge_id),
                    )
                    .one(db)
                    .await;

            if let Err(err) = ret_contest_model {
                log::error!("Database error | Cause: {}", err);

                return Err(ServiceError::new_without_message(
                    ServiceErrorType::InternalServerError,
                ));
            }

            ret_contest_model.unwrap()
        };

        match contest_model {
            Some(contest_model) => {
                let response_entity: entity::Contest =
                    entity::Contest::from_model(contest_model);

                Ok(Some(response_entity))
            }
            None => Ok(None),
        }
    }

    pub async fn find_challenge_by_id(
        db: &DatabaseConnection,
        id: i32,
    ) -> Result<Option<entity::Challenge>, ServiceError> {
        let challenge_model: Option<challenge::Model> = {
            let ret_challenge_model: Result<Option<challenge::Model>, DbErr> =
                Challenge::find()
                    .filter(challenge::Column::Id.eq(id))
                    .one(db)
                    .await;

            if let Err(err) = ret_challenge_model {
                log::error!("Database error | Cause: {}", err);

                return Err(ServiceError::new_without_message(
                    ServiceErrorType::InternalServerError,
                ));
            }

            ret_challenge_model.unwrap()
        };

        match challenge_model {
            Some(challenge_model) => {
                let response_entity: entity::Challenge =
                    entity::Challenge::from_model(challenge_model);

                Ok(Some(response_entity))
            }
            None => Ok(None),
        }
    }

    pub async fn find_contest_by_id(
        db: &DatabaseConnection,
        id: i32,
    ) -> Result<Option<entity::Contest>, ServiceError> {
        let contest_model: Option<contest::Model> = {
            let ret_contest_model: Result<Option<contest::Model>, DbErr> =
                Contest::find()
                    .filter(contest::Column::Id.eq(id))
                    .one(db)
                    .await;

            if let Err(err) = ret_contest_model {
                log::error!("Database error | Cause: {}", err);

                return Err(ServiceError::new_without_message(
                    ServiceErrorType::InternalServerError,
                ));
            }

            ret_contest_model.unwrap()
        };

        match contest_model {
            Some(contest_model) => {
                let response_entity: entity::Contest =
                    entity::Contest::from_model(contest_model);

                Ok(Some(response_entity))
            }
            None => Ok(None),
        }
    }
}
