use models::*;
use sea_orm::{ActiveModelTrait, ActiveValue, DatabaseConnection};

use crate::{
    error::{ServiceError, ServiceErrorType},
    repository::Repository,
};

impl Repository {
    pub async fn delete_challenge_by_id(
        db: &DatabaseConnection,
        challenge_id: i32,
    ) -> Result<(), ServiceError> {
        let challenge_model: challenge::ActiveModel = challenge::ActiveModel {
            id: ActiveValue::Set(challenge_id),
            ..Default::default()
        };

        if let Err(err) = challenge_model.delete(db).await {
            log::error!("Database error | Cause: {}", err);

            return Err(ServiceError::new_without_message(
                ServiceErrorType::InternalServerError,
            ));
        }

        Ok(())
    }

    pub async fn delete_contest_by_id(
        db: &DatabaseConnection,
        contest_id: i32,
    ) -> Result<(), ServiceError> {
        let contest_model: contest::ActiveModel = contest::ActiveModel {
            id: ActiveValue::Set(contest_id),
            ..Default::default()
        };

        if let Err(err) = contest_model.delete(db).await {
            log::error!("Database error | Cause: {}", err);

            return Err(ServiceError::new_without_message(
                ServiceErrorType::InternalServerError,
            ));
        }

        Ok(())
    }
}
