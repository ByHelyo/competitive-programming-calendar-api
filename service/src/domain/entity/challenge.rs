use models::*;

pub struct Challenge {
    pub id: Option<i32>,
    pub name: Option<String>,
}

impl Challenge {
    pub fn new(id: Option<i32>, name: Option<String>) -> Self {
        Self { id, name }
    }

    pub fn from_model(challenge: challenge::Model) -> Self {
        Self {
            id: Some(challenge.id),
            name: Some(challenge.name),
        }
    }

    pub fn from_models(challenge_models: Vec<challenge::Model>) -> Vec<Self> {
        challenge_models.into_iter().map(Self::from_model).collect()
    }
}
