use std::collections::HashMap;

use models::*;
use sea_orm::DatabaseConnection;
use time::OffsetDateTime;

use crate::{
    domain::{
        entity,
        service::{Service, Utils},
    },
    error::ServiceError,
    presentation::dto,
};

pub struct Contest {
    pub id: Option<i32>,
    pub name: Option<String>,
    pub start: Option<OffsetDateTime>,
    pub end: Option<OffsetDateTime>,
    pub duration: Option<i32>,
    pub challenge_id: Option<i32>,
    pub challenge_name: Option<String>,
    pub url: Option<String>,
}

impl Contest {
    pub fn from_model(contest: contest::Model) -> Self {
        Self {
            id: Some(contest.id),
            name: Some(contest.name),
            start: Some(contest.start),
            end: Some(contest.end),
            duration: Some(contest.duration),
            challenge_id: Some(contest.challenge_id),
            challenge_name: None,
            url: Some(contest.url),
        }
    }

    pub fn from_models(contests: Vec<contest::Model>) -> Vec<Self> {
        contests.into_iter().map(Self::from_model).collect()
    }

    pub async fn get_challenge_name_from_challenge_id(
        &mut self,
        db: &DatabaseConnection,
    ) -> Result<(), ServiceError> {
        let mut challenge: entity::Challenge =
            Service::get_challenge_by_id(db, self.challenge_id.unwrap())
                .await?;

        self.challenge_name = challenge.name.take();
        Ok(())
    }

    pub async fn get_challenges_name_from_challenges_id(
        db: &DatabaseConnection,
        challenges: &mut [entity::Challenge],
    ) -> Result<(), ServiceError> {
        let hashmap_challenges_id_name: HashMap<i32, String> =
            Utils::get_map_challenges(db).await?;

        for challenge in challenges.iter_mut() {
            challenge.name = Some(
                hashmap_challenges_id_name
                    .get(&challenge.id.unwrap())
                    .unwrap()
                    .to_owned(),
            );
        }

        Ok(())
    }

    pub async fn from_dto(
        db: &DatabaseConnection,
        contest: dto::Contest,
    ) -> Result<Self, ServiceError> {
        let challenge: entity::Challenge =
            Service::get_challenge_by_name(db, contest.challenge.clone())
                .await?;

        Ok(Self {
            id: None,
            name: Some(contest.name),
            start: Some(contest.start),
            end: Some(contest.end),
            duration: Some(contest.duration),
            challenge_id: Some(challenge.id.unwrap()),
            challenge_name: Some(contest.challenge),
            url: Some(contest.url),
        })
    }
}
