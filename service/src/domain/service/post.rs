use sea_orm::DatabaseConnection;

use crate::{
    domain::{entity, service::Service},
    error::{ServiceError, ServiceErrorType},
    presentation::dto,
    repository::Repository,
};

impl Service {
    pub async fn post_challenge(
        db: &DatabaseConnection,
        challenge_name: String,
    ) -> Result<entity::Challenge, ServiceError> {
        if Repository::find_challenge_by_name(db, challenge_name.clone())
            .await?
            .is_some()
        {
            let error_msg: String =
                format!("The challenge \'{}\' already exists", challenge_name);
            log::error!("{}", error_msg);

            return Err(ServiceError::new(
                ServiceErrorType::BadRequest,
                &error_msg,
            ));
        }

        let challenge_id: i32 =
            Repository::post_challenge(db, challenge_name.clone()).await?;

        Ok(entity::Challenge::new(
            Some(challenge_id),
            Some(challenge_name),
        ))
    }

    pub async fn post_contest(
        db: &DatabaseConnection,
        request_dto: dto::Contest,
    ) -> Result<dto::ContestWithId, ServiceError> {
        let mut entity: entity::Contest =
            entity::Contest::from_dto(db, request_dto).await?;

        if Repository::contest_exist(db, &entity).await?.is_some() {
            let error_msg: String = format!(
                "The contest \'{}\' already exists",
                entity.name.unwrap()
            );

            log::error!("{}", error_msg);

            return Err(ServiceError::new(
                ServiceErrorType::BadRequest,
                &error_msg,
            ));
        }

        let contest_id: i32 = Repository::post_contest(db, &entity).await?;

        entity.id = Some(contest_id);

        Ok(dto::ContestWithId::from_entity(entity))
    }
}
