use sea_orm::DatabaseConnection;

use crate::{
    domain::{entity, service::Service},
    error::ServiceError,
    repository::Repository,
};

impl Service {
    pub async fn delete_challenge_by_id(
        db: &DatabaseConnection,
        challenge_id: i32,
    ) -> Result<entity::Challenge, ServiceError> {
        let response_entity: entity::Challenge =
            Service::get_challenge_by_id(db, challenge_id).await?;

        Repository::delete_challenge_by_id(db, challenge_id).await?;

        Ok(response_entity)
    }

    pub async fn delete_contest_by_id(
        db: &DatabaseConnection,
        contest_id: i32,
    ) -> Result<entity::Contest, ServiceError> {
        let response_entity: entity::Contest =
            Service::get_contest_by_id(db, contest_id).await?;

        Repository::delete_contest_by_id(db, contest_id).await?;

        Ok(response_entity)
    }
}
