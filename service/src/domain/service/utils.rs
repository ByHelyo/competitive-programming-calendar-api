use std::collections::HashMap;

use sea_orm::DatabaseConnection;

use crate::{
    domain::{
        entity,
        service::{Service, Utils},
    },
    error::ServiceError,
};

impl Utils {
    pub async fn get_map_challenges(
        db: &DatabaseConnection,
    ) -> Result<HashMap<i32, String>, ServiceError> {
        let challenges: Vec<entity::Challenge> =
            Service::get_challenges(db).await?;

        Ok(challenges
            .into_iter()
            .map(|challenge| (challenge.id.unwrap(), challenge.name.unwrap()))
            .collect())
    }
}
