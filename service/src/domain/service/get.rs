use sea_orm::DatabaseConnection;

use crate::{
    domain::{entity, service::Service},
    error::{ServiceError, ServiceErrorType},
    repository::Repository,
};

impl Service {
    pub async fn get_challenges(
        db: &DatabaseConnection,
    ) -> Result<Vec<entity::Challenge>, ServiceError> {
        let challenges: Vec<entity::Challenge> =
            Repository::find_challenges(db).await?;

        Ok(challenges)
    }

    pub async fn get_challenge_by_id(
        db: &DatabaseConnection,
        challenge_id: i32,
    ) -> Result<entity::Challenge, ServiceError> {
        let response_entity: entity::Challenge = {
            let ret_response_entity: Option<entity::Challenge> =
                Repository::find_challenge_by_id(db, challenge_id).await?;

            if ret_response_entity.is_none() {
                let error_msg: String =
                    format!("Challenge id \'{}\' does not exist", challenge_id);

                log::error!("{}", error_msg);

                return Err(ServiceError::new(
                    ServiceErrorType::BadRequest,
                    &error_msg,
                ));
            }

            ret_response_entity.unwrap()
        };

        Ok(response_entity)
    }

    pub async fn get_contests(
        db: &DatabaseConnection,
    ) -> Result<Vec<entity::Contest>, ServiceError> {
        let mut contests: Vec<entity::Contest> =
            Repository::find_contests(db).await?;

        for contest in contests.iter_mut() {
            contest.get_challenge_name_from_challenge_id(db).await?;
        }

        Ok(contests)
    }

    pub async fn get_contest_by_id(
        db: &DatabaseConnection,
        contest_id: i32,
    ) -> Result<entity::Contest, ServiceError> {
        let mut response_entity: entity::Contest = {
            let ret_response_entity: Option<entity::Contest> =
                Repository::find_contest_by_id(db, contest_id).await?;

            if ret_response_entity.is_none() {
                let error_msg: String =
                    format!("Contest id \'{}\' does not exist", contest_id);

                log::error!("{}", error_msg);

                return Err(ServiceError::new(
                    ServiceErrorType::BadRequest,
                    &error_msg,
                ));
            }

            ret_response_entity.unwrap()
        };

        response_entity
            .get_challenge_name_from_challenge_id(db)
            .await?;

        Ok(response_entity)
    }

    pub async fn get_challenge_by_name(
        db: &DatabaseConnection,
        challenge_name: String,
    ) -> Result<entity::Challenge, ServiceError> {
        let response_entity: entity::Challenge = {
            let response_entity: Option<entity::Challenge> =
                Repository::find_challenge_by_name(db, challenge_name.clone())
                    .await?;

            if response_entity.is_none() {
                let error_msg: String = format!(
                    "Challenge id \'{}\' does not exist",
                    challenge_name
                );

                log::error!("{}", error_msg);

                return Err(ServiceError::new(
                    ServiceErrorType::BadRequest,
                    &error_msg,
                ));
            }

            response_entity.unwrap()
        };

        Ok(response_entity)
    }
}
