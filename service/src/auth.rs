use actix_web::dev::ServiceRequest;
use actix_web_httpauth::extractors::{
    bearer::{BearerAuth, Config},
    AuthenticationError,
};
use alcoholic_jwt::{
    token_kid, validate, ValidJWT, Validation, ValidationError, JWK, JWKS,
};
use reqwest::Response;

use crate::error::{ServiceError, ServiceErrorType};

pub async fn validator(
    req: ServiceRequest,
    credentials: BearerAuth,
) -> Result<ServiceRequest, (actix_web::Error, ServiceRequest)> {
    let config: Config = req.app_data::<Config>().cloned().unwrap_or_default();

    match validate_token(credentials.token()).await {
        Ok(res) => {
            if res {
                Ok(req)
            } else {
                Err((AuthenticationError::from(config).into(), req))
            }
        }
        Err(err) => Err((err.into(), req)),
    }
}

async fn validate_token(token: &str) -> Result<bool, ServiceError> {
    let authority: String = utils::get_env("AUTHORITY");

    let jwks: JWKS = fetch_jwks(&format!(
        "{}{}",
        authority.as_str(),
        ".well-known/jwks.json"
    ))
    .await?;

    let validations: Vec<Validation> =
        vec![Validation::Issuer(authority), Validation::SubjectPresent];

    let kid: Result<Option<String>, ValidationError> = token_kid(token);

    if let Err(err) = kid {
        log::error!("Failed to decode token headers | Cause: {}", err);

        return Err(ServiceError::new_without_message(
            ServiceErrorType::InternalServerError,
        ));
    }

    let kid: Option<String> = kid.unwrap();

    if kid.is_none() {
        log::error!("No 'kid' claim present in token");

        return Err(ServiceError::new_without_message(
            ServiceErrorType::InternalServerError,
        ));
    }

    let kid: String = kid.unwrap();

    let jwk: Option<&JWK> = jwks.find(&kid);

    if jwk.is_none() {
        log::error!("Specified key not found in set");

        return Err(ServiceError::new_without_message(
            ServiceErrorType::InternalServerError,
        ));
    }

    let jwk: &JWK = jwk.unwrap();

    let res: Result<ValidJWT, ValidationError> =
        validate(token, jwk, validations);

    Ok(res.is_ok())
}

async fn fetch_jwks(uri: &str) -> Result<JWKS, ServiceError> {
    let res: Result<Response, reqwest::Error> = reqwest::get(uri).await;

    if let Err(err) = res {
        log::error!("Failed to fetch jwks | Cause: {}", err);

        return Err(ServiceError::new_without_message(
            ServiceErrorType::InternalServerError,
        ));
    }

    let res: Response = res.unwrap();

    let val: Result<JWKS, reqwest::Error> = res.json().await;

    if let Err(err) = val {
        log::error!("Failed to deserialize | Cause: {}", err);

        return Err(ServiceError::new_without_message(
            ServiceErrorType::InternalServerError,
        ));
    }

    let val: JWKS = val.unwrap();

    Ok(val)
}
