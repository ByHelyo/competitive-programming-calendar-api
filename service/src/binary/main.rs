mod config;

use actix_web::{middleware::Logger, web, App, HttpServer};
use actix_web_httpauth::middleware::HttpAuthentication;
use sea_orm::DatabaseConnection;
use service::{auth, presentation::resource::Resource, state::AppState};

use crate::config::Config;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    dotenv::dotenv().ok();

    let config: Config = Config::default();

    let db: DatabaseConnection = utils::setup_database(&config.db_url).await;

    let state: AppState = AppState { db };

    HttpServer::new(move || {
        let logger: Logger = Logger::default();
        let auth = HttpAuthentication::bearer(auth::validator);

        App::new()
            .wrap(logger)
            .app_data(web::Data::new(state.clone()))
            .route("/", web::head().to(Resource::head))
            .service(
                web::scope("/api")
                    .route(
                        "/challenges",
                        web::get().to(Resource::get_challenges),
                    )
                    .route(
                        "/challenge/{id}",
                        web::get().to(Resource::get_challenge_by_id),
                    )
                    .route("/contests", web::get().to(Resource::get_contests))
                    .route(
                        "/contest/{id}",
                        web::get().to(Resource::get_contest_by_id),
                    ),
            )
            .service(
                web::scope("/admin")
                    .wrap(auth)
                    .route(
                        "/challenge",
                        web::post().to(Resource::post_challenge),
                    )
                    .route(
                        "/challenge/{id}",
                        web::delete().to(Resource::delete_challenge_by_id),
                    )
                    .route("/contest", web::post().to(Resource::post_contest))
                    .route(
                        "/contest/{id}",
                        web::delete().to(Resource::delete_contest_by_id),
                    ),
            )
    })
    .bind(config.server_url)?
    .run()
    .await?;

    Ok(())
}
