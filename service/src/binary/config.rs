pub struct Config {
    pub db_url: String,
    pub server_url: String,
}

impl Default for Config {
    fn default() -> Self {
        Self::config_env()
    }
}

impl Config {
    fn config_env() -> Self {
        let db_url: String = utils::get_env("DB_URL_CONTESTS");
        let host: String = utils::get_env("SERVICE_HOST");
        let port: String = utils::get_env("SERVICE_PORT");
        let server_url = format!("{}:{}", host, port);

        Config { db_url, server_url }
    }
}
