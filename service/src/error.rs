use actix_web::{error::ResponseError, http::StatusCode, HttpResponse};
use derive_more::Display;
use serde::Serialize;

#[derive(Debug, Display)]
pub enum ServiceErrorType {
    #[display(fmt = "internal server error")]
    InternalServerError,
    #[display(fmt = "bad request")]
    BadRequest,
}

#[derive(Debug, Display)]
#[display(fmt = "{:?}", self)]
pub struct ServiceError {
    error: ServiceErrorType,
    message: Option<String>,
}

#[derive(Serialize)]
struct ServiceErrorResponse {
    code: u16,
    error: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    message: Option<String>,
}

impl ServiceError {
    pub fn new_without_message(error: ServiceErrorType) -> Self {
        ServiceError {
            error,
            message: None,
        }
    }

    pub fn new(error: ServiceErrorType, name: &str) -> Self {
        ServiceError {
            error,
            message: Some(name.to_string()),
        }
    }
}

impl ResponseError for ServiceError {
    fn error_response(&self) -> HttpResponse {
        let error_response: ServiceErrorResponse = ServiceErrorResponse {
            code: self.status_code().as_u16(),
            error: self.error.to_string(),
            message: self.message.clone(),
        };

        HttpResponse::build(self.status_code()).json(error_response)
    }

    fn status_code(&self) -> StatusCode {
        match &self.error {
            ServiceErrorType::InternalServerError { .. } => {
                StatusCode::INTERNAL_SERVER_ERROR
            }
            ServiceErrorType::BadRequest { .. } => StatusCode::BAD_REQUEST,
        }
    }
}
