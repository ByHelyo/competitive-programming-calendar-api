use sea_orm_migration::prelude::*;

use crate::migrator::m20220101_000001_create_challenge_table::Challenge;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(Contest::Table)
                    .col(
                        ColumnDef::new(Contest::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Contest::Name).string().not_null())
                    .col(
                        ColumnDef::new(Contest::Start)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(Contest::End)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(ColumnDef::new(Contest::Duration).integer().not_null())
                    .col(ColumnDef::new(Contest::Url).string().not_null())
                    .col(
                        ColumnDef::new(Contest::ChallengeId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .name("fk-contest-challenge_id")
                            .from(Contest::Table, Contest::ChallengeId)
                            .to(Challenge::Table, Challenge::Id),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Contest::Table).to_owned())
            .await
    }
}

#[derive(Iden)]
pub enum Contest {
    Table,
    Id,
    Name,
    Start,
    End,
    Duration,
    Url,
    ChallengeId,
}
