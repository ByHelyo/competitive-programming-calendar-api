pub struct Config {
    pub db_url: String,
}

impl Default for Config {
    fn default() -> Self {
        Self::config_env()
    }
}

impl Config {
    fn config_env() -> Self {
        let db_url: String = utils::get_env("DB_URL_CONTESTS");

        Config { db_url }
    }
}
