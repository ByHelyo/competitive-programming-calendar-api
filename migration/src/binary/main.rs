mod config;

use futures::executor::block_on;
use migration::migrator::Migrator;
use sea_orm::{Database, DatabaseConnection, DbErr};
use sea_orm_migration::prelude::*;

use crate::config::Config;

async fn setup_database(config: Config) -> DatabaseConnection {
    let db: Result<DatabaseConnection, DbErr> =
        Database::connect(config.db_url).await;

    match db {
        Ok(db) => db,
        Err(err) => {
            log::error!("Connection to the database failed | Cause: {}", err);
            std::process::exit(1);
        }
    }
}

async fn migration(db: DatabaseConnection) {
    match Migrator::refresh(&db).await {
        Ok(()) => (),
        Err(err) => {
            log::error!("Migration failed | Cause: {}", err);
            std::process::exit(1);
        }
    }
}

fn main() {
    env_logger::init();
    dotenv::dotenv().ok();

    let config: Config = Config::default();

    let db: DatabaseConnection = block_on(setup_database(config));

    block_on(migration(db));

    log::info!("Migration performed successfully");
}
